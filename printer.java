package test;

public class hello {

	public static void main(String[] args) {
	//	IPrinter str = new PrinterDefault();
	//	str.print1("hola");
		
	//	IPrinter str2 = new PrinterSpecial();
	//	str2.print1("hola");
		
	//	Word ww = new Word("Privet");
	//	IPrinter pp = new PrinterDefault();
	//	ww.Print(pp);
		
		Text txt = new Text(new Word("Hola"), new Sign(','), new Sign(' '), new Word("Mundo"), new Sign('!'), new Sign('\n'));
		
		IPrinter pt = new PrinterDefault();
		txt.Print(pt);
		
		IPrinter ps = new PrinterSpecial();
		txt.Print(ps);
		
		IPrinterDelegate ptd = new PrinterDelegate();
		ptd.PrntD(txt);
		
	}

}

interface IPrinter {
	void print1(String s);
	void print2(char c);
}

class PrinterDefault implements IPrinter {
	public void print1(String s) {
		System.out.print(s);
	}
	
	public void print2(char c) {
		System.out.print(c);
	}
}

class PrinterSpecial extends PrinterDefault {
	public void print1(String s) {
		System.out.print("("+s+")");
	}
}

interface IPrintable{
	void Print(IPrinter p);    //"Soy imprimible, para eso me tienes que decir como me tienes que dar una imresora"
}

class Word implements IPrintable {
	private String w;
	public Word(String s) {
		this.w = s;
	}
	public void Print(IPrinter p) {  //"Soy imprimible, me tienes que dar una impresora de la imprenta"
		p.print1(this.w);             // "ok, he recibido la impresora print1 de la imprenta p"
	}
}

class Sign implements IPrintable {
	private char w;
	public Sign(char s) {
		this.w = s;
	}
	public void Print(IPrinter p) {
		p.print2(this.w);
	}
}

class Text implements IPrintable {
	private IPrintable[] arr;
	public Text(IPrintable... _arr) {
		this.arr = _arr;
	}
	public void Print(IPrinter ptr) {  //ptr es IPrinter, t.e. eto sposob kotorim my budem pechatat, eto argument dlia IPrintable
		for(IPrintable pp : arr) {
			pp.Print(ptr);       	   // Print pues dependiendo de si es sign o word Print automaticamente se convertira en print1 o print2
		}
	}                           	   //"yo imprimo todo lo que me das, word o sign, y los imprimo segun el metodo definido para word o sign"
}

interface IPrinterDelegate extends IPrinter {
	void PrntD(IPrintable obj);
}

class PrinterDelegate extends PrinterDefault implements IPrinterDelegate {
	public void PrntD(IPrintable o) {
		o.Print(this);
	}
}
